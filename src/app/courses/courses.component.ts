import { Component, OnInit, Renderer, ElementRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MainService } from '../services/main.service';

export interface Courses {
  active: boolean;
  name: string;
}

@Component({
  selector: 'app-courses',
  templateUrl: './courses.component.html',
  styleUrls: ['./courses.component.scss']
})
export class CoursesComponent implements OnInit {
  courses: any = [];
  tipo: any = [];
  area: any = [];


  activeType: string = '';
  activeArea: string = '';

  clases: any;

  default: boolean = true;
  src: boolean = false;

  localData: any;
  localType: any;
  localArea: any;
  localCourses: any = [];

  constructor(private mainService: MainService,
              private route: ActivatedRoute,
              private renderer: Renderer,
              private elem: ElementRef) {

    this.activeType = this.route.snapshot.params['type'];
    this.activeArea = this.route.snapshot.params['area'];

    this.mainService.getJSON().subscribe(data => {
      this.localData = data;
      this.localType = data.types[this.activeType];
      this.localArea = this.localType.areas[this.activeArea];
      this.localCourses = Object.values(this.localArea.courses);
      // console.log(this.localCourses);
    });
  }

  ngOnInit() {

    this.mainService.getCourses(this.activeType, this.activeArea).valueChanges().subscribe( (courses: Courses[]) => {
      if (courses && courses.length !== 0) {
        this.courses = courses.filter(curso => curso.active);
      } else {
        this.courses = this.localCourses.filter(curso => curso.active);
        // console.log(this.courses);
      }
    });

    this.mainService.getTipo(this.activeType).valueChanges().subscribe(tipo => {
      this.tipo = tipo ? tipo : this.localType;
    });

    this.mainService.getArea(this.activeType, this.activeArea).valueChanges().subscribe(area => {
      this.area = area ? area : this.localArea;
    });

    this.mainService.getClass().valueChanges().subscribe( clases => {
      this.clases = clases;
    });
  }

  filterCouses(event, id) {
    const clicked = event.target || event.srcElement || event.currentTarget;
    this.removeActiveClass();
    clicked.classList.add('active');

    this.mainService.getCourses(this.activeType, this.activeArea).valueChanges().subscribe( (courses: any[]) => {
      this.courses = courses.filter(course => course.clase === id);
    });
  }

  resetFilter(event) {
    const clicked = event.target || event.srcElement || event.currentTarget;
    this.removeActiveClass();
    clicked.classList.add('active');
    this.mainService.getCourses(this.activeType, this.activeArea).valueChanges().subscribe(courses => {
      this.courses = courses;
    });
  }

  removeActiveClass() {
    const allFilters = this.elem.nativeElement.querySelectorAll('.item');
    for (let i = 0; i < allFilters.length; i++) {
      allFilters[i].classList.remove('active');
    }
  }

  load() {
    this.default = false;
    this.src = true;
  }
}
