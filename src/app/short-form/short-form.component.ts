import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { debounceTime, map } from 'rxjs/operators';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MainService } from '../services/main.service';

@Component({
  selector: 'app-short-form',
  templateUrl: './short-form.component.html',
  styleUrls: ['./short-form.component.css']
})
export class ShortFormComponent implements OnInit {
  public model: any;
  rForm: FormGroup;
  titleAlert: string = 'Completa este campo';
  courses: any = [];
  partnerId: string = '1';
  noProgram: boolean = false;
  showLoader: boolean = false;
  constructor(private mainService: MainService, private fb: FormBuilder, private route: ActivatedRoute) {
    this.partnerId = this.route.snapshot.queryParams['partnerId'] || '1';
  }

  ngOnInit() {
    this.mainService.getAllCourses().valueChanges().subscribe(courses => {
      this.courses = courses;
    });

    this.model = { name: null };

    this.rForm = this.fb.group({
      'nombre': [null, Validators.compose([
        Validators.required,
        Validators.minLength(3),
        Validators.maxLength(140)
      ])],
      'celular': [null, Validators.compose([
        Validators.required,
        Validators.minLength(7),
        Validators.maxLength(10)
      ])],
      'correo': [null, Validators.compose([
        Validators.required,
        Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$')
      ])],
      'id': [null],
      'periodo': [null],
      'tyc': [true, Validators.compose([
        Validators.required
      ])]
    });
  }
  sendData(data) {
    data.partnerId = this.partnerId;
    if (this.model.name) {
      this.showLoader = true;
      data.programa = this.model.name;
      this.mainService.sendData(data);
    } else {
      this.showLoader = false;
      this.noProgram = true;
    }
  }

  search = (text$: Observable<string>) =>
    text$.pipe(
      debounceTime(200),
      map(term => term === '' ? []
        : this.courses.filter(v => v.name.toLowerCase().indexOf(term.toLowerCase()) > -1).slice(0, 10))
    )

  formatter = (x: { name: string }) => x.name;
}
