import { Component, OnInit } from '@angular/core';
import {
  trigger,
  state,
  style,
  animate,
  transition,
  query,
  stagger
} from '@angular/animations';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  animations: [
    trigger('showMenu', [
      state('show', style({
        display: 'block',
        transform: 'translateY(0)',
        opacity: 1,
      })),
      state('hide', style({
        display: 'none',
        transform: 'translateY(-10px)',
        opacity: 0,
      })),
      transition('show => hide', animate('100ms ease-out')),
      transition('hide => show', animate('200ms ease-in'))
    ])
  ]
})
export class HeaderComponent implements OnInit {

  loggedUser: any = null;


  constructor() {

  }

  ngOnInit() {

  }


}
