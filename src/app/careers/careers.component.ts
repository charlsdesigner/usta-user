import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationEnd } from '@angular/router';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { Type } from '../interfaces/create';
import { MainService } from '../services/main.service';

@Component({
  selector: 'app-careers',
  templateUrl: './careers.component.html',
  styleUrls: ['./careers.component.scss']
})
export class CareersComponent implements OnInit {

  tipo: any = [];
  activeTipo: string;

  areas: any;
  ruta: string;

  default: boolean = true;
  src: boolean = false;
  localData: any;
  localType: any;
  localAreas: any;


  constructor(private mainService: MainService, private route: ActivatedRoute, location: Location, router: Router) {

    router.events.subscribe((event) => {
      if ((event instanceof NavigationEnd)) {
        this.activeTipo = this.getActiveRoute();
        this.getType();
        this.getAreas();
      }
    });

    this.mainService.getJSON().subscribe(data => {
      this.localData = data;
      this.localType = data.types[this.activeTipo];
      this.localAreas = Object.values(this.localType.areas);
    });

  }

  getActiveRoute() {
    return this.route.snapshot.params['type'];
  }

  getType() {
    this.mainService.getTipo(this.activeTipo).valueChanges().subscribe(tipo => {
      this.tipo = tipo ? tipo : this.localType;
    });
  }

  getAreas() {
    this.mainService.getAreas(this.activeTipo).valueChanges().subscribe(areas => {
      if (areas && areas.length !== 0) {
        this.areas = areas;
      } else {
        this.areas = this.localAreas;
      }
    });
  }

  ngOnInit() {
  }

  load() {
    this.default = false;
    this.src = true;
  }

}
