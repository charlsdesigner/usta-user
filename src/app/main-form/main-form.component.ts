import { Component, OnInit, Input } from '@angular/core';
import { Observable } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { debounceTime, map } from 'rxjs/operators';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MainService } from '../services/main.service';

@Component({
  selector: 'app-main-form',
  templateUrl: './main-form.component.html',
  styleUrls: ['./main-form.component.scss']
})
export class MainFormComponent implements OnInit {

  @Input() course: string;

  public model: any;

  courses: any = [];
  partnerId: string = '1';
  rForm: FormGroup;
  noProgram: boolean = false;
  titleAlert: string = 'Completa este campo';
  showLoader: boolean = false;
  constructor(private mainService: MainService, private fb: FormBuilder, private route: ActivatedRoute) {
    this.partnerId = this.route.snapshot.queryParams['partnerId'] || '1';
  }

  ngOnInit() {
    this.mainService.getAllCourses().valueChanges().subscribe(courses => {
      this.courses = courses;

      setTimeout(() => {
        this.model = { name: this.course };
      }, 2000);
    });

    this.rForm = this.fb.group({
      'nombre': [null, Validators.compose([
        Validators.required,
        Validators.minLength(3),
        Validators.maxLength(140)
      ])],
      'celular': [null, Validators.compose([
        Validators.required,
        Validators.minLength(7),
        Validators.maxLength(10)
      ])],
      'correo': [null, Validators.compose([
        Validators.required,
        Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$')
      ])],
      'id': [null],
      'periodo': [null],
      'tyc': [true, Validators.compose([
        Validators.required
      ])]
    });
  }

  sendData(data) {
    if (this.model.file) {
      data.file = this.model.file.id;
    } else {
      data.file = false;
    }
    if (this.model.name) {
      this.showLoader = true;
      data.programa = this.model.name;
      this.mainService.sendData(data);
    } else {
      this.noProgram = true;
      this.showLoader = false;
    }
  }

  search = (text$: Observable<string>) =>
    text$.pipe(
      debounceTime(200),
      map(term => term === '' ? []
        : this.courses.filter(v => v.name.toLowerCase().indexOf(term.toLowerCase()) > -1).slice(0, 10))
    )

  formatter = (x: { name: string }) => x.name;

}
