import { Component, OnInit, Renderer, ElementRef } from '@angular/core';
import { SwUpdate } from '@angular/service-worker';
import { MessaginService } from './services/messagin.service';
import { Router, NavigationEnd, NavigationStart, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  loader: boolean = false;
  loaderState: string = '5%';

  message: any = {};
  showNot: boolean = false;

  constructor(private element: ElementRef,
              private renderer: Renderer,
              private router: Router,
              private route: ActivatedRoute,
              private swUpdate: SwUpdate,
              private msgService: MessaginService) {

    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        (<any>window).ga('set', 'page', event.urlAfterRedirects);
        (<any>window).ga('send', 'pageview');
      }
    });
  }

  ngOnInit(): void  {
    if (this.swUpdate.isEnabled) {
      this.swUpdate.available.subscribe((v) => {
        if (confirm('Actualización disponible, deseas obtenerla?')) {
          window.location.reload();
        }
      });
    }

    this.msgService.getPermission();
    this.msgService.receiveMessage();
    const message = this.msgService.currentMessage;

    message.subscribe( m => {
      console.log(m);
      if (m != null) {
        this.message = m.notification;
        this.showNot = true;
        console.log(this.message);
      }
    });

    const navbar: HTMLElement = this.element.nativeElement.children[0].children[0];
    this.renderer.listenGlobal('window', 'scroll', (event) => {
      const number = window.scrollY;
      if (number > 80 || window.pageYOffset > 80) {
        // add logic
        navbar.classList.remove('header-transparent');
      } else {
        // remove logic
        navbar.classList.add('header-transparent');
      }
    });

    this.router.events.subscribe((event) => {
      window.scrollTo(0, 0);

      if ((event instanceof NavigationStart)) {
        this.loader = true;
      } else if ((event instanceof NavigationEnd)) {
        setTimeout(() => {
          this.loaderState = '100%';
        }, 300);
        setTimeout(() => {
          this.loader = false;
        }, 900);
      }
    });

  }
}
