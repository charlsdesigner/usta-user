import { Injectable } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database/database';
import { Http } from '@angular/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Router, ActivatedRoute } from '@angular/router';


@Injectable({
  providedIn: 'root'
})
export class MainService {

  constructor(private afDB: AngularFireDatabase,
              private http: Http,
              private router: Router,
              private route: ActivatedRoute) { }

  public getJSON(): Observable<any> {
    return this.http.get('./assets/api/ustaDB.json')
      .pipe(map((res: any) => res.json()));
  }
  // Areas
  public getAreas(type) {
    return this.afDB.list('data/types/' + type + '/areas');
  }
  public getArea(type, area) {
    return this.afDB.object('data/types/' + type + '/areas/' + area);
  }
  // Clases
  public getClass() {
    return this.afDB.list('data/clases/');
  }

  // Tipos
  public getTipos() {
    return this.afDB.list('data/types/');
  }
  public getTipo(id) {
    return this.afDB.object('data/types/' + id);
  }

  // Cursos
  public getCourses(type, area) {
    return this.afDB.list('data/types/' + type + '/areas/' + area + '/courses');
  }

  public getAllCourses() {
    return this.afDB.list('data/allcourses');
  }

  public getCourse(type, area, id) {
    return this.afDB.object('data/types/' + type + '/areas/' + area + '/courses/' + id);
  }

  public getFileUrl(id) {
    return this.afDB.object('data/uploads/files/' + id);
  }

  public getTestimonials() {
    return this.afDB.list('data/testimonials');
  }

  // Send Data
  public sendData(data) {
    let fileUrl = 'confirmed';
    if (data.file) {
      fileUrl = data.file;
    }

    //const URL = `https://ares.3dm.com.co/bobm/Views/WS/?campaignId=21&type=Direct&partnerId=${data.partnerId}&nombre=${data.nombre}&id=${data.id}&celular=${data.celular}&email=${data.correo}&programa=${data.programa}&periodo=${data.periodo}`;

    const URL_BMBO = `https://bmdigital.co/usta-bo/bmbo.php?campaignId=21&type=Direct&partnerId=${data.partnerId}&nombre=${data.nombre}&id=${data.id}&celular=${data.celular}&email=${data.correo}&programa=${data.programa}&periodo=${data.periodo}`;

    // const URL_MILLENIUM = `https://centrocontacto.usta.edu.co/millesantotomas/rest/brandMedia?name=${data.nombre}&email=${data.correo}&phone=${data.celular}&id=${data.id}&program=${data.programa}&period=${data.periodo}`;

    const URL_MILLENIUM = `https://bmdigital.co/usta-bo/millbo.php?nombre=${data.nombre}&email=${data.correo}&celular=${data.celular}&id=${data.id}&programa=${data.programa}&periodo=${data.periodo}`;


    this.http.get(URL_MILLENIUM).toPromise()
      .then(
        res => {
          console.log(res);
        },
        msg => {
          console.log(msg);
        }
      );

    this.http.get(URL_BMBO).toPromise()
    .then(
      res => {
        console.log(res);
      },
    msg => {
      console.log(msg);
    }
    );

    this.router.navigate(['gracias/' + fileUrl]);

    window.location.reload();

  }

}
