import { Injectable } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database/database';
import { AngularFireAuth } from 'angularfire2/auth/auth';
import * as firebase from 'firebase';
import { take } from 'rxjs/operators';
import 'rxjs';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MessaginService {

  currentMessage = new BehaviorSubject(null);

  constructor(private afDB: AngularFireDatabase, private afAuth: AngularFireAuth) {}

  updateToken(token) {
    this.afAuth.authState.subscribe(user => {
      const data = { [token]: token };
      this.afDB.database.ref('fcmTokens/').update(data);
    });
  }

  getPermission() {
    this.registerAnonimusUser();
    if (firebase.messaging.isSupported()) {
      const messaging = firebase.messaging();
      messaging.requestPermission()
        .then(() => {
          return messaging.getToken();
        })
        .then(token => {
          console.log(token);
          this.updateToken(token);
        })
        .catch((err) => {
          console.log('Unable to get permission to notify.', err);
        });
    }
  }

  receiveMessage() {
    if (firebase.messaging.isSupported()) {
      const messaging = firebase.messaging();
      messaging.onMessage((payload) => {
        this.currentMessage.next(payload);
      });
    }
  }

  public registerAnonimusUser() {
    firebase.auth().signInAnonymously()
    .then( function (response) {
      const uid = response.user.uid;
      firebase.auth().onAuthStateChanged(function (user) {
        if (user) {
          // User is signed in.
          var isAnonymous = user.isAnonymous;
          var uid = user.uid;

          console.log('loged');
          // ...
        } else {
          // User is signed out.
          // ...
          console.log('no loged');
        }
        // ...
      });
    })
    .catch(function (error) {
      console.log(error);
      // ...
    });
  }

  // public AddUser(uid) {
  //   this.afDB.database.ref('anonimusUsers/' + uid).set(uid)
  //     .then((res) => {
  //       console.log(res);
  //     })
  //     .catch((error) => {
  //       console.log(error);
  //     });
  // }
}
