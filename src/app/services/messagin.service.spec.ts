import { TestBed, inject } from '@angular/core/testing';

import { MessaginService } from './messagin.service';

describe('MessaginService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MessaginService]
    });
  });

  it('should be created', inject([MessaginService], (service: MessaginService) => {
    expect(service).toBeTruthy();
  }));
});
