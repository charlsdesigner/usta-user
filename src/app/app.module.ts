import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HttpModule } from '@angular/http';

// Material
import {
  MatExpansionModule,
  MatMenuModule,
  MatButtonModule,
  MatProgressBarModule,
  MatProgressSpinnerModule,
  MatSnackBarModule
} from '@angular/material';
// Firebase

import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFireStorageModule } from 'angularfire2/storage';

// Servicios
import { MainService } from './services/main.service';
import { MessaginService } from './services/messagin.service';


import { AppComponent } from './app.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { HeaderComponent } from './header/header.component';
import { CoursesComponent } from './courses/courses.component';
import { FiltersPipe } from './pipes/filters.pipe';
import { HomeComponent } from './home/home.component';
import { FooterComponent } from './footer/footer.component';
import { CareersComponent } from './careers/careers.component';
import { MainFormComponent } from './main-form/main-form.component';
import { DetailComponent } from './detail/detail.component';
import { TestimoniesComponent } from './testimonies/testimonies.component';
import { SerchComponent } from './serch/serch.component';
import { ShortFormComponent } from './short-form/short-form.component';
import { ThanksComponent } from './thanks/thanks.component';
import { ClasesPipe } from './pipes/clases.pipe';
import { ContactComponent } from './contact/contact.component';



// Routes
const appRoutes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'inicio', component: HomeComponent},
  { path: 'carreras/:type', component: CareersComponent},
  { path: 'carreras/:type/:area', component: CoursesComponent },
  { path: 'carreras/:type/:area/:course', component: DetailComponent },
  { path: 'contactanos', component: ContactComponent},
  { path: 'gracias/:id', component: ThanksComponent},
];

// Firebase Config
export const firebaseConfig = {
  apiKey: 'AIzaSyCnzHBuJ-RPEmiBx40JG5rsTx6VT4iiUW0',
  authDomain: 'usantoto-693aa.firebaseapp.com',
  databaseURL: 'https://usantoto-693aa.firebaseio.com',
  projectId: 'usantoto-693aa',
  storageBucket: 'usantoto-693aa.appspot.com',
  messagingSenderId: '1007761166932'
};


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    CoursesComponent,
    FiltersPipe,
    HomeComponent,
    FooterComponent,
    CareersComponent,
    MainFormComponent,
    DetailComponent,
    TestimoniesComponent,
    SerchComponent,
    ShortFormComponent,
    ThanksComponent,
    ClasesPipe,
    ContactComponent
  ],
  imports: [
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireDatabaseModule,
    AngularFireStorageModule,
    AngularFireAuthModule,
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    NgbModule,
    MatExpansionModule,
    MatButtonModule,
    MatMenuModule,
    HttpModule,
    ReactiveFormsModule,
    RouterModule.forRoot(appRoutes, { useHash: true }),
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production })
  ],
  entryComponents: [],
  providers: [ MainService, MessaginService ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
