import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { ActivatedRoute } from '@angular/router';
import { MainService } from '../services/main.service';
import { NgbModalConfig, NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss']
})
export class DetailComponent implements OnInit {
  course: any = [];
  tipo: any = [];
  area: any = [];
  videoUrl: any;
  dangerousVideoUrl: any;

  activeType: string = '';
  activeArea: string = '';
  activeCourse: string = '';

  contenido: any = [];


  courseName: string;

  default: boolean = true;
  src: boolean = false;

  localCourse: any;

  constructor(private mainService: MainService,
              private route: ActivatedRoute,
              config: NgbModalConfig, private modalService: NgbModal,
              protected sanitizer: DomSanitizer) {
    config.keyboard = false;
    this.activeType = this.route.snapshot.params['type'];
    this.activeArea = this.route.snapshot.params['area'];
    this.activeCourse = this.route.snapshot.params['course'];

    this.mainService.getJSON().subscribe( res => {
      this.localCourse = res;
    });
  }
  ngOnInit() {

    this.mainService.getCourse(this.activeType, this.activeArea, this.activeCourse).valueChanges().subscribe(course => {
      if (course) {
        this.course = course;
      } else {
        this.course = this.localCourse.types[this.activeType].areas[this.activeArea].courses[this.activeCourse];
        console.log(this.course);
      }
      this.updateVideoUrl(this.course.video);
      this.contenido = this.course.content;

      setTimeout(() => {
        this.courseName = this.course.name;
      }, 100);
    });

    this.mainService.getTipo(this.activeType).valueChanges().subscribe(tipo => {
      this.tipo = tipo ? tipo : this.localCourse.types[this.activeType];
    });

    this.mainService.getArea(this.activeType, this.activeArea).valueChanges().subscribe(area => {
      this.area = area ? area : this.localCourse.types[this.activeType].areas[this.activeArea];
    });
  }


  updateVideoUrl(id: string) {
    this.dangerousVideoUrl = 'https://www.youtube.com/embed/' + id;
    this.videoUrl =
      this.sanitizer.bypassSecurityTrustResourceUrl(this.dangerousVideoUrl);
  }

  open(content) {
    this.modalService.open(content, { centered: true, windowClass: 'form-modal', size: 'lg' });
  }
  load() {
    this.default = false;
    this.src = true;
  }
}
