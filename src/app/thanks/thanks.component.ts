import { Component, OnInit } from '@angular/core';
import { MainService } from '../services/main.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-thanks',
  templateUrl: './thanks.component.html',
  styleUrls: ['./thanks.component.css']
})
export class ThanksComponent implements OnInit {

  fileUrl: any;
  id: string = '';

  constructor(private mainService: MainService, private route: ActivatedRoute) {
    this.id = this.route.snapshot.params['id'];
    (<any>window).ga('set', 'page', '/gracias');
    (<any>window).ga('send', 'pageview');
  }

  ngOnInit() {
    this.mainService.getFileUrl(this.id).valueChanges().subscribe( file => {
      this.fileUrl = file;
    });
  }

}
