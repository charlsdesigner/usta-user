export interface Item {
    title: string;
    content: string;
}

export interface Area {
    id: any;
    name?: string;
    img?: string;
}

export interface Type {
    id: any;
    name?: string;
    color?: string;
    img?: string;
}

export interface Class {
    id: any;
    name: string;
}

export interface Course {
    name: string;
    acreditacion: string;
    area: string;
    content?: Item;
    duracion: string;
    inversion: string;
    jornada: string;
    modalidad: string;
    picture: string;
    resgistro: string;
    snies: string;
    tipo: string;
    clase?: string;
    titulo: string;
    ubicacion: string;
    url: string;
    video: string;
}
