import { Component, OnInit, Input } from '@angular/core';
import { MainService } from '../services/main.service';

export interface Testimonio {
  name: string;
  content: string;
  pic: string;
  active: boolean;
  area: string;
  curso: string;
  id: string;
  tipo: string;
}

@Component({
  selector: 'app-testimonies',
  templateUrl: './testimonies.component.html',
  styleUrls: ['./testimonies.component.scss']
})
export class TestimoniesComponent implements OnInit {

  @Input() course: string;

  testimonials: Testimonio[] = [];

  constructor(private mainService: MainService) { }

  ngOnInit() {
    this.mainService.getTestimonials().valueChanges().subscribe( (testimonials: Testimonio[]) => {
      this.testimonials = testimonials.filter( testimonio => testimonio.curso === this.course);
    });
  }

}
