import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { debounceTime, map } from 'rxjs/operators';
import { MainService } from '../services/main.service';
import { NgbModalConfig, NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-serch',
  templateUrl: './serch.component.html',
  styleUrls: ['./serch.component.scss']
})
export class SerchComponent implements OnInit {
  public model: any;

  courses: any = [];
  localCourses: any;

  showInp: boolean = true;

  constructor(private mainService: MainService, config: NgbModalConfig, private modalService: NgbModal) { 
    config.keyboard = false;
    this.mainService.getJSON().subscribe( res => {
      this.localCourses = Object.values(res.allcourses);
    });
  }

  ngOnInit() {
    this.mainService.getAllCourses().valueChanges().subscribe( courses => {
      this.courses = courses ? courses : this.localCourses;
    });
  }

  open(content) {
    this.showInp = false;
    const modal = this.modalService.open(content, { centered: true, windowClass: 'search-modal' });
    modal.result.then(() => { this.showInp = true; }, () => { this.showInp = true; });
  }

  search = (text$: Observable<string>) =>
    text$.pipe(
      debounceTime(200),
      map(term => term === '' ? []
        : this.courses.filter(v => v.name.toLowerCase().indexOf(term.toLowerCase()) > -1).slice(0, 10))
    )

  formatter = (x: { name: string }) => x.name;

}
