<?php
header("Access-Control-Allow-Origin: *");
header('Access-Control-Allow-Methods: GET, POST');
header("Access-Control-Allow-Headers: X-Requested-With");
header('Content-Type: text/html; charset=utf-8');

$id = $_GET['id'];
$celular = $_GET['celular'];
$nombre = $_GET['nombre'];
$email = $_GET['email'];
$programa = $_GET['programa'];
$periodo = $_GET['periodo'];

$url='https://centrocontacto.usta.edu.co/millesantotomas/rest/brandMedia?name='.urlencode($nombre).'&phone='.urlencode($celular).'&email='.urlencode($email).'&program='.urlencode($programa).'&id='.urlencode($id).'&period='.urlencode($periodo);
$curl_handle=curl_init();
curl_setopt($curl_handle, CURLOPT_URL,$url);
curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 30);
curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($curl_handle, CURLOPT_USERAGENT, 'Title');
$query = curl_exec($curl_handle);
curl_close($curl_handle);

echo "Reply: $query";

?>