// import * as functions from 'firebase-functions';
// import * as admin from 'firebase-admin';

// admin.initializeApp();

// The Cloud Functions for Firebase SDK to create Cloud Functions and setup triggers.
const functions = require('firebase-functions');

// The Firebase Admin SDK to access the Firebase Realtime Database.
const admin = require('firebase-admin');
admin.initializeApp();

exports.sendAdminNotification = functions.database.ref('/data/allcourses/{pushId}').onWrite((change, context) => {
    const course = change.after.val();
    console.log(course);
    if (!course.new) return null;
    return Promise.all([admin.database().ref(`/fcmTokens/`).once('value')]).then(results => {
        const tokens = results[0];
        if (!tokens.hasChildren()) return null;
        const payload = {
            notification: {
                title: 'Nuevo curso disponible',
                body: `${course.name}`,
                icon: 'https://usantoto-693aa.firebaseapp.com/assets/icons/icon-128x128.png',
                click_action: `carreras/${course.tipo}/${course.area}/${course.url}`
            }
        };
        const tokensList = Object.keys(tokens.val());
        const sortList = tokensList.sort().slice(1, 1000);
        console.log('Mensaje enviado a ' + sortList.length, + ' usuarios', payload);
        return admin.messaging().sendToDevice(sortList, payload);
    });
});


exports.sendCustomNotification = functions.database.ref('/data/pushmessages/{pushId}').onWrite((change, context) => {
    const message = change.after.val();
    return Promise.all([admin.database().ref(`/fcmTokens/`).once('value')]).then(results => {
        const tokens = results[0];
        if (!tokens.hasChildren()) return null;
        const payload = {
            notification: {
                title: `${message.title}`,
                body: `${message.content}`,
                icon: 'https://usantoto-693aa.firebaseapp.com/assets/icons/icon-128x128.png',
                click_action: `${message.action}`
            }
        };
        const tokensList = Object.keys(tokens.val());
        const sortList = tokensList.sort().slice(1, 1000);
        console.log('Mensaje enviado a ' + sortList.length, + ' usuarios', payload);
        return admin.messaging().sendToDevice(sortList, payload);
    });
});